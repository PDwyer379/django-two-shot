from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib import messages
from datetime import datetime


# Create your views here.
@login_required
def show_receipts(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt_list,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    accounts = Account.objects.filter(owner=request.user)

    if request.method == "POST":
        form = ReceiptForm(request.POST)

        form.fields["category"].queryset = categories
        form.fields["account"].queryset = accounts

        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.date = datetime.now()
            receipt.save()
            messages.success(request, "Receipt created successfully")
            return redirect("home")
    else:
        form = ReceiptForm()
        form.fields["category"].queryset = categories
        form.fields["account"].queryset = accounts
    context = {"form": form}

    return render(request, "receipts/create_receipt.html", context)


@login_required
def category_list(request):
    category_view = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_view": category_view,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    account_view = Account.objects.filter(owner=request.user)
    context = {
        "account_view": account_view,
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
